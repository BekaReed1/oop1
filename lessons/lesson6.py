# База данных
# судб- система управления базданных
# slq -- язык структурированных запросов
# NOsql
# oracle , posgreSQL ,MySQL
# SQLite3
# CRUD-create reed update delete
import sqlite3

db = sqlite3.connect('py32.db')
c = db.cursor()

c.execute(''' CREATE TABLE IF NOT EXISTS user(
fullname TEXT,
title TEXT,
view INTEGER,
dateB DATE
)
''')

# create in student
# c.execute(''' INSERT INTO user VALUES('BEKA','пишу стихи',0,'2003-06-07') ''')
c.execute('''INSERT INTO user VALUES ('Дмитрий','ментор',100,'1983-09-07')''')

# update
c.execute('''UPDATE user SET fullname='гик' WHERE rowid=1  ''')
c.execute('''UPDATE user SET view=100 WHERE fullname="гик" ''')
c.execute('''UPDATE user SET title="я очень умный" WHERE rowid>2''')
c.execute('''UPDATE user SET fullname="None" WHERE not rowid=4 ''')

# delete
c.execute('''DELETE FROM user WHERE view <> 100''')
c.execute(''' DELETE FROM user WHERE fullname="None"''')

# reed
c.execute('''SELECT rowid,* FROM user ''')
# print(c.fetchone())
# print(c.fetchmany(2))
item = c.fetchall()
for i in item:
    print(i)

db.commit()
db.close()
