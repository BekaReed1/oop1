# ООП
#
def a1(a, b):
    return a + b


# print(a(2, 3))
# print(a(3, 4))
# print(a(2, 32))
#
# a = 'str', True, 1, 1.9, [], {}, ()
#
# print(type(a))


# метод - функция внутри класса
# магический метод - __ def __
# cвойства -переменные внутри класса
# магичесике методы-
# экземпляр класса - переменная класса


class Car:
    a = 'колеса'
    b = 'руль'

    def __init__(self, color, name, age):  # конструктор
        self.color = color
        self.name = name
        self.age = age

    def __str__(self):
        return f'name is {self.name}, {self.color} {self.age}\n' \
               f'{self.a} {self.b}'

    def __len__(self):
        return len(self.name)

    def run(self):
        print(f'{self.name} is run ')


car = Car('black', 'BMW', 2001)
car2 = Car('black', 'brabus', 2001)
car3 = Car('black', 'sonata', 2001)


print(car)
car.run()
car2.run()
car3.run()
print(car2)
print(len(car2))
# run(car)
# print(type(car))
