# 3 принципа
# 1 наследование  2 полиморфизм
# 3 __ин_капсуляция
# 3 побличный защищенный скрытый

class A:
    a = '1'

    def __init__(self, n):
        self.__n = n


class B(A):
    _a = 2

b = B('n')
b._a = 3
b._A__n=1

# print(b._a)
print(dir(b))
print(b._A__n)

