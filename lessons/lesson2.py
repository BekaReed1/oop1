# принципы ооп
# 4 Наследование Полиморфизм Инкапсуляция Абстракция

class A:  # суперкласс родительский класс
    name = 'Алишер'


object1 = A
print(object1.name)


class B(A):  # дочерний класс

    def a(self):
        print(self.name, 'этот человек стал миллионером')


object2 = B()

object2.a()


class FRank(B):
    name = 'Жасмина'

    def __init__(self, tryname, last):
        self.tryn = tryname
        self.last = last

    def a(self):
        print(f'{self.tryn} теперь бедный')


frank = FRank('Beka', '')
frank.a()


class Human(FRank):
    name = 'Дмитрий'

    def __init__(self, tryname, last, money):
        FRank.__init__(self, tryname, last)
        super().__init__(tryname, last)

        self.money = money

    def a(self):
        B.a(self)
        super().a()


hum1 = Human('Дмитрий', '', '1Million')
hum1.a()
B.a(hum1)


#
# def name(a):
#     print(a)
#
# name('Алмаз')

# print(Human.mro())

# 1 публичный 2 защищенный 3 скрытый
class Bank:
    def __init__(self, name, last, sKEY, money):
        self.name = name
        self.last = last
        self._key = sKEY
        self.__money = money

    def _secter(self):
        print(self._key,self.__money)

    def cnopka(self):
        self._secter()


kr1 = Bank('Ахмед', 'великий', 'q12345rf3r', 1_000_000)
kr1.cnopka()

kr1._key = 'qwertyuio12345'

kr1._secter()

kr1._Bank__money=0
kr1.cnopka()

print(dir(kr1))
#__name   _class__name