import art


# from art import tprint
#
# tprint('beka#########')
# import django
#
# django.setup()


# множественное наследование
# MRO metod resolution order
# порядок выполнения методов

class A:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def art(self):
        print('это метод класса A')


class A2(A):
    def a(self):
        print('это метод класса А2')


class B:
    def __init__(self, b2):
        self.b2 = b2

    def art(self):
        print('это метод класса В')


class C(B, A2):
    def __init__(self, b2, a, b):
        B.__init__(self,b2)
        A.__init__(self, a, b)

    def art(self):
        print('это метод класса C')


c = C('b2','a','b')
c.art()
print(C.mro())