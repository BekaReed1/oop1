import sqlite3
from sqlite3 import Error


# CRUD
# C create
def create_student(conn, student):
    sql = '''INSERT INTO student(full_name,mark,hobby,b_date,is_married)
     VALUES (?,?,?,?,?)'''
    try:
        cursor = conn.cursor()
        cursor.execute(sql, student)
        conn.commit()
    except Error as e:
        print(e)


# R-Reed

def reed_student(conn):
    try:
        sql = '''SELECT * FROM student'''
        cursor = conn.cursor()
        cursor.execute(sql)
        rows = cursor.fetchall()

        for i in rows:
            print(i)

    except Error as e:
        print(e)


# U-Update
def update_name_mark(conn, id, fullname, mark):
    sql = '''UPDATE student SET full_name=?,mark=? WHERE id=?'''
    try:
        cursor = conn.cursor()
        cursor.execute(sql, (fullname, mark, id))
        conn.commit()
    except Error as e:
        print(e)


# U-Update
def update_date_isM(conn, id, date, married):
    sql = '''UPDATE student SET b_date=?,is_married=? WHERE id=?'''
    try:
        cursor = conn.cursor()
        cursor.execute(sql, (date, married, id))
        conn.commit()
    except Error as e:
        print(e)


# D-Delete
def delete_student(conn, id):
    sql = '''DELETE FROM student WHERE id=? '''
    try:
        cursor = conn.cursor()
        cursor.execute(sql,(id))
        conn.commit()
    except Error as e:
        print(e)


def create_connection(db_file):
    conn = False
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)
    return conn


def create_table(conn, sql):
    try:
        cursor = conn.cursor()
        cursor.execute(sql)
    except Error as e:
        print(e)


sql_table = '''CREATE TABLE IF NOT EXISTS student(
id INTEGER PRIMARY KEY AUTOINCREMENT,
full_name VARCHAR(50) NOT NULL,
mark REAL NOT NULL DEFAULT 0,
hobby TEXT DEFAULT NULL,
b_date DATE NOT NULL,
is_married BOOLEAN DEFAULT FALSE
);
'''

database = r'group32.db'
connection = create_connection(database)
if connection is not None:
    print('всё работает')
    create_table(connection, sql_table)
    # create_student(connection, ('name', 5.3, 'True', '2008-09-09', False))
    update_name_mark(connection, 4, 'beka', 3.4)
    update_date_isM(connection, 2, '2003-09-09', True)
    delete_student(connection, 8)
    # connection.execute('''DELETE FROM student WHERE id=?''')
    reed_student(connection)
